package other;

import receiver.Receive;
import shipper.Ship;

import java.io.BufferedReader;
import java.io.FileReader;

public class Main
{
	private static HumanResource humanResource;
	private static Receive receive;
	private static Ship ship;

	public static void main(String[] args)
	{
		System.out.println("Welcome to the warehouse!");
		initialize();
		try
		{
			String currentDIR = new java.io.File(".").getCanonicalPath();
			System.out.println("Current directory:" + currentDIR);
			//check on internet abt () is right or not
			BufferedReader reader = new BufferedReader(new FileReader("input.txt"));
			String line = reader.readLine();
			while (line != null)
			{
				if (line.length() > 0)
					executeInstruction(line);
				line = reader.readLine();
			}
			reader.close();
		}
		catch (Exception e)
		{
			System.out.println("Error working with filesystem: " + e.getMessage());
		}

		System.out.println("Human resources reports " + humanResource.getTotalMinutesWorked() + " minutes worked.");
	}

	private static void initialize()
	{
		humanResource = new HumanResource();
		ship = new Ship(humanResource);
		receive = new Receive(humanResource);
	}

	private static void executeInstruction(String line)
	{
		// Split the string into parts separated by " "
		String[] wordPart = line.split(" ");
		if (wordPart[0].equals("RECEIVE"))
			receive.obtain(wordPart[1], Integer.parseInt(wordPart[2]));
		else if (wordPart[0].equals("SHIP"))
			ship.ship(wordPart[1], wordPart[2], Integer.parseInt(wordPart[3]));
	}
}
