package other;

import receiver.ForkliftOperator;
import shipper.Driver;
import shipper.Picker;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class HumanResource
{
	private int nextDriver;
	private int nextForkLiftOperator;
	private int nextPicker;
	private final List<Employee> drivers;
	private final List<Employee> forkLiftOperators;
	private final List<Employee> pickers;

	public HumanResource()
	{
		nextDriver = 0;
		nextForkLiftOperator = 0;
		nextPicker = 0;
		drivers = new ArrayList<>();
		drivers.add(new Driver());
		drivers.add(new Driver());
		drivers.add(new Driver());
		drivers.add(new Driver());
		drivers.add(new Driver());
		forkLiftOperators = new ArrayList<>();
		forkLiftOperators.add(new ForkliftOperator());
		forkLiftOperators.add(new ForkliftOperator());
		forkLiftOperators.add(new ForkliftOperator());
		forkLiftOperators.add(new ForkliftOperator());
		forkLiftOperators.add(new ForkliftOperator());
		pickers = new ArrayList<>();
		pickers.add(new Picker());
		pickers.add(new Picker());
		pickers.add(new Picker());
		pickers.add(new Picker());
		pickers.add(new Picker());
	}

	public Driver getDriver()
	{
		Driver driver = (Driver)drivers.get(nextDriver);
		nextDriver = (nextDriver + 1) % 5;
		return driver;
	}

	public Picker getPicker()
	{
		Picker picker = (Picker)pickers.get(nextPicker);
		nextPicker = (nextPicker + 1) % 5;
		return picker;
	}

	public ForkliftOperator getForkLiftOperator()
	{
		ForkliftOperator forkliftOperator = (ForkliftOperator)forkLiftOperators.get(nextForkLiftOperator);
		nextForkLiftOperator = (nextForkLiftOperator + 1) % 5;
		return forkliftOperator;
	}

	public int getTotalMinutesWorked()
	{
		int count = 0;
		Iterator<Employee> iterator = drivers.iterator();
		while (iterator.hasNext())
		{
			count += iterator.next().getMinutesWorkedSinceLastPay();
		}
		iterator = pickers.iterator();
		while (iterator.hasNext())
		{
			count += iterator.next().getMinutesWorkedSinceLastPay();
		}
		iterator = forkLiftOperators.iterator();
		while (iterator.hasNext())
		{
			count += iterator.next().getMinutesWorkedSinceLastPay();
		}
		return count;
	}
}
