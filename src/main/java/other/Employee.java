package other;

public class Employee
{
	private int minutesWorkedSinceLastPay;

	public Employee()
	{
		minutesWorkedSinceLastPay = 0;
	}

	protected void logTime(int minutes) {
		// Add minutes to minutes_worked_since_last_pay
		minutesWorkedSinceLastPay += minutes;
	}

	public int getMinutesWorkedSinceLastPay()
	{
		return minutesWorkedSinceLastPay;
	}
}
