package receiver;

import other.Employee;

public class ForkliftOperator extends Employee
{
	public void storeItem(String item, int count)
	{
		logTime(20 * count);
		System.out.println("Stored " + count + " " + item);
	}
}
