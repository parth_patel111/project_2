package receiver;

import other.HumanResource;

public class Receive
{
	private HumanResource humanResource;

	public Receive(HumanResource humanResource)
	{
		this.humanResource = humanResource;
	}

	public void obtain(String item, int count)
	{
		ForkliftOperator forkliftOperator = humanResource.getForkLiftOperator();
		forkliftOperator.storeItem(item, count);
		System.out.println("Received " + count + " " + item);
	}
}
