package shipper;

import other.Employee;

public class Picker extends Employee
{
	public void retrieve(String item, int count)
	{
		logTime(5 * count);
		System.out.println("Picked " + count + " " + item);
	}
}
