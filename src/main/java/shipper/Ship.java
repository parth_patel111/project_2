package shipper;

import other.HumanResource;

public class Ship
{
	private HumanResource humanResource;

	public Ship(HumanResource humanResource)
	{
		this.humanResource = humanResource;
	}

	public void ship(String item, String account, int count)
	{
		Picker picker = humanResource.getPicker();
		picker.retrieve(item, count);
		Driver driver = humanResource.getDriver();
		driver.deliver(account, item, count);
		System.out.println("Shipped " + count + " " + item + " to " + account);
	}
}
