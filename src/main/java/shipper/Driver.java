package shipper;

import other.Employee;

public class Driver extends Employee
{
	public void deliver(String account, String item, int count)
	{
		// C is the count
		logTime(15 * count);
		System.out.println("Delivered " + item + " to " + account);
	}
}
